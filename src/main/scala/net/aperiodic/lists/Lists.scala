package net.aperiodic.lists

object Lists {

  def last[T](ls : List[T]) : T = {
    ls match {
      case Nil      => throw new NoSuchElementException
      case h :: Nil => h
      case _ :: t   => last(t)
    }
  }

  def lastButOne[T](ls : List[T]) : T = {
    ls match {
      case Nil           => throw new NoSuchElementException
      case h :: Nil      => throw new NoSuchElementException
      case h :: _ :: Nil => h
      case _ :: t        => lastButOne(t)
    }
  }

  def nth[T](pos : Int, ls : List[T]) : T =
    (pos, ls) match {
      case (_, Nil)    => throw new NoSuchElementException
      case (0, h :: _) => h
      case (_, _ :: t) => nth(pos - 1, t)
    }

  def length[T](ls : List[T]) : Int =
    // TODO make tail-recursive !!
    ls match {
      case Nil    => 0
      case _ :: t => length(t) + 1
    }

  def reverse[T](ls : List[T]) : List[T] = {
    // TODO make tail-recursive !!
    ls match {
      case Nil       => Nil
      case h :: tail => reverse(tail) ::: List[T](h)
    }
  }

  def isPalindrome[A](ls : List[A]) : Boolean = {
    // XXX this solution is stupidly inefficient :-(
    // XXX should be ls === ls.reverse (which is still inefficient but better than this
    ls match {
      case Nil      => true
      case h :: Nil => true
      case h :: t => (h, t.reverse.head) match {
        case (x, y) => if (x equals y) isPalindrome(t.reverse.tail) else false
      }
    }
  }

  def flatten(ls : List[Any]) : List[Any] =
    ls.flatMap {
      case ms : List[_] => flatten(ms)
      case e            => List(e)
    }

  def compress[A](ls : List[A]) : List[A] = {
    ls match {
      case Nil       => Nil
      case h :: tail => h :: compress(tail.dropWhile(_ == h))
    }
  }

  def pack[A](ls : List[A]) : List[List[A]] = {
    ls match {
      case Nil => Nil
      case lsr => lsr.span(_ == lsr.head) match {
        case (l, r) => (l) :: (pack(r))
      }
    }
  }

  def encode[A](ls : List[A]) : List[(Int, A)] =
    pack(ls).map(gr => (gr.length, gr.head))

  def encode11[A](ls : List[A]) : List[Any] =
    pack(ls).map(gr => if (gr.length == 1) gr.head else (gr.length, gr.head))

  def decode[A](ls : List[(Int, A)]) : List[A] =
    ls.flatMap(gr => List.fill(gr._1)(gr._2))

  def encodeDirect[A](ls : List[A]) : List[(Int, A)] = {
    ls match {
      case Nil => Nil
      case lsr => lsr.span(_ == lsr.head) match {
        case (l, r) => (l.length, l.head) :: (encodeDirect(r))
      }
    }
  }

  def duplicate[A](ls : List[A]) = {
    { for (x <- ls) yield x :: x :: Nil }.flatMap(x => x)
  }

}