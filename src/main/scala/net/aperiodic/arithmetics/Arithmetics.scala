package net.aperiodic.arithmetics

import java.math.BigInteger

class S99Int(val start : Int) {
  import S99Int._

  def isPrime() : Boolean = {
    new BigInteger(start.toString).isProbablePrime(10)
  }
}

object S99Int {
  implicit def int2S99Int(i : Int) : S99Int = new S99Int(i)
}
